const {
  app,
  BrowserWindow,
  Menu,
  MenuItem,
  Tray,
  dialog
} = require("electron");
const url = require("url");
const path = require("path");


let win;

function createWindow() {
  win = new BrowserWindow({ width: 375, height: 600 });
  win.loadURL(
    url.format({
      pathname: path.join(__dirname, "index.html"),
      protocol: "file:",
      slashes: true
    })
  );
  win.webContents.openDevTools();
  let trayIcon = new Tray(path.join("", "trayicon.png"));
  let trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
  trayIcon.setContextMenu(trayMenu);
}

const trayMenuTemplate = [
  {
    label: "Empty Application",
    enabled: false
  },

  {
    label: "Settings",
    click: function() {
      console.log("Clicked on settings");
    }
  },

  {
    label: "Help",
    click: function() {
      console.log("Clicked on Help");
    }
  }
];

const template = [
  {
    label: "File",
    submenu: [
      {
        label: "Settings",
        role: "Settings"
      },
      {
        label: "Exit",
        role: "Exit"
      }
    ]
  },
  {
    label: "Edit",
    submenu: [
      {
        role: "undo"
      },
      {
        role: "redo"
      },
      {
        type: "separator"
      },
      {
        role: "cut"
      },
      {
        role: "copy"
      },
      {
        role: "paste"
      }
    ]
  },

  {
    role: "help",
    submenu: [
      {
        label: "About"
      }
    ]
  }
];

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);

app.on("ready", createWindow);
